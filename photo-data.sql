/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/SQLTemplate.sql to edit this template
 */
/**
 * Author:  00827843
 * Created: May 19, 2023
 */

insert into "PHOTOS" ("title","description","filename","owner") values ('Photo #1','A photo of a house','house.jpeg','M. Nilsen');
insert into "PHOTOS" ("title","description","filename","owner") values ('Photo #2','A photo of a cat','cat.jpeg','M. Nilsen');

