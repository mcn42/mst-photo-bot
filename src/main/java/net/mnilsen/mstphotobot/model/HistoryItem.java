/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.mnilsen.mstphotobot.model;

import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import java.util.Date;

/**
 *
 * @author michaeln
 */
public class HistoryItem extends BaseEntity {
    private Date postDate;
    private PostStatus status;
    
    @ManyToOne
    private Photo photo;
}
