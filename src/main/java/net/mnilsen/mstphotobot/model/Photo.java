/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.mnilsen.mstphotobot.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import java.util.Date;
import org.springframework.context.annotation.Bean;

/**
 *
 * @author 00827843
 */
@Entity(name="Photo")
@Table(name="PHOTOS")
public class Photo extends BaseEntity {
    private Date addedOn;
    private Date lastPosted;
    private int postCount = 0;
    private String location;
    private String path;
    private String filename;
    private String title;
    private String description;
    private String keywords;
    private String license;
    private String owner;

    public Photo() {
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Date getLastPosted() {
        return lastPosted;
    }

    public void setLastPosted(Date lastPosted) {
        this.lastPosted = lastPosted;
    }

    public int getPostCount() {
        return postCount;
    }

    public void setPostCount(int postCount) {
        this.postCount = postCount;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getFilename() {
        return filename;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }


    @Override
    public String toString() {
        return "Photo{" + "id=" + id + ", lastPosted=" + lastPosted + ", filename=" + filename + ", title=" + title + '}';
    }
    
    
}
