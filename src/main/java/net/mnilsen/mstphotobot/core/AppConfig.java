package net.mnilsen.mstphotobot.core;

/**
 *
 * @author michaeln
 */
public class AppConfig {
    public static String mastodonAccountUrl = "";
    public static String mastodonAccountKey = "";
    public static String mastodonAccountSecret = "";
    
    public static String photoBaseDirectory;
    //  Valid range 1-48
    public static int postPeriodHours;
    //  Valid range 0-60
    public static int postPeriodJitterMinutes;
    
}
