/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.mnilsen.mstphotobot.core;

import jakarta.annotation.PostConstruct;
import java.util.List;
import net.mnilsen.mstphotobot.dao.TestDataGenerator;
import net.mnilsen.mstphotobot.dao.PhotoRepository;
import net.mnilsen.mstphotobot.model.Photo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author michaeln
 */
@RestController
@EnableJpaRepositories(basePackageClasses=PhotoRepository.class)
public class RootController {
    @Autowired
    private PhotoRepository photoRepo;
    
    @Value("${welcome.message}")
    private String message = ":(";
    
    @PostConstruct
    public void postConstruct() {
        List<Photo> list = TestDataGenerator.getTestPhotos();
        list.forEach((p) -> this.photoRepo.save(p));
    }
    
    @GetMapping("/")
    public String getRootResource(ModelMap model) {
        StringBuilder sb = new StringBuilder();
        Iterable<Photo> iter = this.photoRepo.findAll();
        iter.forEach((p)->sb.append(p.toString()).append("\n") );
        return sb.toString();
    }
}
