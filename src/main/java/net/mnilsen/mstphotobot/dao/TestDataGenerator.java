/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.mnilsen.mstphotobot.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import net.mnilsen.mstphotobot.model.Photo;

/**
 *
 * @author 00827843
 * 
 *  private Date addedOn;
    private Date lastPosted;
    private int postCount = 0;
    private String location;
    private String filename;
    private String title;
    private String description;
    private String keywords;
    private String license;
    private String owner;
 */
public class TestDataGenerator {
    public static List<Photo> getTestPhotos() {
        List<Photo> list = new ArrayList<>();
        Photo p = new Photo();
        p.setAddedOn(new Date());
        p.setLocation("40.000000,  -74.000000");
        p.setPath("/photos");
        p.setFilename("cat.jpg");
        p.setTitle("A Cat Picture");
        p.setDescription("A picture of a cat");
        p.setKeywords("cats, photographs");
        p.setLicense("https://creativecommons.org/licenses/by-sa/4.0/legalcode");
        p.setOwner("MCN");
        list.add(p);
        p = new Photo();
        p.setAddedOn(new Date());
        p.setLocation("40.000000,  -74.000000");
        p.setPath("/photos");
        p.setFilename("house.jpg");
        p.setTitle("A House Picture");
        p.setDescription("A picture of a house");
        p.setKeywords("architecture, photographs");
        p.setLicense("https://creativecommons.org/licenses/by-sa/4.0/legalcode");
        p.setOwner("MCN");
        list.add(p);
        
        return list;
        
    }
}
