/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package net.mnilsen.mstphotobot.dao;

import java.io.Serializable;
import net.mnilsen.mstphotobot.model.Photo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author 00827843
 * @param <P>
 * @param <L>
 */
@Repository
public interface PhotoRepository<P extends Photo,L extends Serializable> extends JpaRepository<Photo,Long> {
}
